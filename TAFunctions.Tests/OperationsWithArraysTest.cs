using System;
using NUnit.Framework;

namespace TAFunctions
{
    [TestFixture]
    public class OperationsWithArraysTest
    {
        [Test]
        [TestCase(null, SortOrder.Ascending)]
        [TestCase(null, SortOrder.Descending)]
        public void IsSorted_NullArgumentArraySort_ThrowArgumentException(int[] array, SortOrder order)
        {
            Assert.Throws<ArgumentNullException>(() =>
                    OperationsWithArrays.IsSorted(array, order));
        }

        [Test]
        [TestCase(new int[] { 10, 20, 30 }, SortOrder.Ascending, ExpectedResult = true)]
        [TestCase(new int[] { 30, 20, 10 }, SortOrder.Descending, ExpectedResult = true)]
        [TestCase(new int[] { 30, 30, 30 }, SortOrder.Ascending, ExpectedResult = true)]
        [TestCase(new int[] { 30, 30, 30 }, SortOrder.Descending, ExpectedResult = true)]
        [TestCase(new int[] { 10, 20, 30 }, SortOrder.Descending, ExpectedResult = false)]
        [TestCase(new int[] { 30, 20, 10 }, SortOrder.Ascending, ExpectedResult = false)]
        [TestCase(new int[] { 30, -7, 30 }, SortOrder.Ascending, ExpectedResult = false)]
        [TestCase(new int[] { 90, 30, 100 }, SortOrder.Descending, ExpectedResult = false)]
        public bool IsSorted_NotNullArraySorted_ResultReturned(int[] array, SortOrder order) =>
            OperationsWithArrays.IsSorted(array, order);

        [Test]
        public void SortArray_CheckAscending_ArraySortedAscending()
        {
            int[] actual1 = new int[] { -3, 0, 0, 10, 20, 20, 30 };
            int[] actual2 = new int[] { 30, 20, -3, 10, 20, 0, 0 };
            int[] actual3 = new int[] { -100, -100, -100, -100 };
            int[] actual4 = new int[] { 0, 0, 0, 0 };
            int[] actual5 = new int[] { };
            int[] expected12 = new int[] { -3, 0, 0, 10, 20, 20, 30 };
            int[] expected3 = new int[] { -100, -100, -100, -100 };
            int[] expected4 = new int[] { 0, 0, 0, 0 };
            int[] expected5 = new int[] { };

            OperationsWithArrays.SortArray(actual1, SortOrder.Ascending);
            OperationsWithArrays.SortArray(actual2, SortOrder.Ascending);
            OperationsWithArrays.SortArray(actual3, SortOrder.Ascending);
            OperationsWithArrays.SortArray(actual4, SortOrder.Ascending);
            OperationsWithArrays.SortArray(actual5, SortOrder.Ascending);

            CollectionAssert.AreEqual(expected12, actual1, "1");
            CollectionAssert.AreEqual(expected12, actual2, "2");
            CollectionAssert.AreEqual(expected3, actual3, "3");
            CollectionAssert.AreEqual(expected4, actual4, "4");
            CollectionAssert.AreEqual(expected5, actual5, "5");
        }

        [Test]
        public void SortArray_CheckDescending_ArraySortedDescending()
        {
            int[] actual1 = new int[] { 30, 20, 20, 10, 0, 0, -3 };
            int[] actual2 = new int[] { 30, 20, -3, 10, 20, 0, 0 };
            int[] actual3 = new int[] { -100, -100, -100, -100 };
            int[] actual4 = new int[] { 0, 0, 0, 0 };
            int[] actual5 = new int[] { };
            int[] expected12 = new int[] { 30, 20, 20, 10, 0, 0, -3 };
            int[] expected3 = new int[] { -100, -100, -100, -100 };
            int[] expected4 = new int[] { 0, 0, 0, 0 };
            int[] expected5 = new int[] { };

            OperationsWithArrays.SortArray(actual1, SortOrder.Descending);
            OperationsWithArrays.SortArray(actual2, SortOrder.Descending);
            OperationsWithArrays.SortArray(actual3, SortOrder.Descending);
            OperationsWithArrays.SortArray(actual4, SortOrder.Descending);
            OperationsWithArrays.SortArray(actual5, SortOrder.Descending);


            CollectionAssert.AreEqual(expected12, actual1, "1");
            CollectionAssert.AreEqual(expected12, actual2, "2");
            CollectionAssert.AreEqual(expected3, actual3, "3");
            CollectionAssert.AreEqual(expected4, actual4, "4");
            CollectionAssert.AreEqual(expected5, actual5, "5");
        }
    }
}